#ifndef _COMMON_HPP_
#define _COMMON_HPP_

#include <glm/glm.hpp>

typedef glm::vec2 Vec2f;
typedef glm::vec3 Col3f;

#endif
