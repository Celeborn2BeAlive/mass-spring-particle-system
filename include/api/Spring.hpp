#ifndef _SPRING_HPP_
#define _SPRING_HPP_

 #include "Particle.hpp"

class Spring {
public:
    virtual ~Spring() {
    }

    virtual void generateForce(Particle* p1) {
        generateForces(p1, 0);
    }

    virtual void generateForces(Particle* p1, Particle* p2) = 0;
};

#endif
