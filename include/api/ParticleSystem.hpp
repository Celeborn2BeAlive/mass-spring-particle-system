#ifndef _PARTICLESYSTEM_HPP_
#define _PARTICLESYSTEM_HPP_

#include <vector>
#include <memory>
#include "Particle.hpp"
#include "Spring.hpp"

class ParticleSystem {
public:
    void addParticle(const Particle& particle) {
        m_Particles.emplace_back(particle);
    }
    
    void addSpring(Spring* spring) {
        m_Springs.emplace_back(spring);
    }

private:
    std::vector<Particle> m_Particles;
    std::vector<Spring*> m_Springs;
};

#endif
