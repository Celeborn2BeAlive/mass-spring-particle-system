#ifndef _SOLVER_HPP_
#define _SOLVER_HPP_

#include "Particle.hpp"

class Solver {
public:
    virtual ~Solver() {
    }
    
    //! Solve numerically the differential equation dp.velocity / dt = p.mass * p.force 
    //! and update the position / velocity of the particle.
    virtual void solve(float dt, Particle& p) = 0;
};

#endif
