#ifndef _PARTICLE_HPP_
#define _PARTICLE_HPP_

#include "common.hpp"

class Particle {
public:
    Vec2f position;
    Vec2f velocity;
    Col3f color;
    float mass;

    Vec2f force; // Force accumulator

    Particle(const Vec2f& position, const Vec2f& velocity, const Col3f& color, float mass):
        position(position), velocity(velocity), color(color), mass(mass), force(0.f, 0.f) {
    }
};

#endif
