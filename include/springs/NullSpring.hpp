#ifndef _NULLSPRING_HPP_
#define _NULLSPRING_HPP_

#include "api/Spring.hpp"

class NullSpring: public Spring {
public:
    virtual ~NullSpring() {

    }

    virtual void generateForces(Particle* p1, Particle* p2) {
        // do nothing
    }
};

#endif

