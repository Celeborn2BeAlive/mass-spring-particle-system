#ifndef _GLRENDERER_HPP_
#define _GLRENDERER_HPP_

#include <GL/glew.h>
#include "api/Particle.hpp"

class GLRenderer {
public:
    GLRenderer(GLfloat massScaleFactor = 0.05f);

    ~GLRenderer();

    void render(const Particle* particles, size_t count) const;

private:
    static const GLuint POSITION_ATTRIB_LOCATION = 0;

    GLfloat m_MassScaleFactor; // The size of a particle is m_MassScaleFactor times its mass
    GLuint m_ParticleVBO;
    GLuint m_ParticleVAO;
    GLuint m_ParticleProgram;
    GLint m_ParticleColorUniformLocation;
    GLint m_ParticlePositionUniformLocation;
    GLint m_ParticleScaleUniformLocation;
};

#endif
