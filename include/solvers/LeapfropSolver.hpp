#ifndef _LEAPFROGSOLVER_HPP_
#define _LEAPFROGSOLVER_HPP_

#include "api/Solver.hpp"

class LeapfrogSolver: public Solver {
public:
    virtual ~LeapfrogSolver() {
    }

    //! Solve numerically the differential equation dp.velocity / dt = p.mass * p.force
    //! and update the position / velocity of the particle.
    virtual void solve(float dt, Particle& p) {
        p.velocity += dt * p.force / p.mass; // v(t + dt) = v(t) + dt * F / m
        p.position += dt * p.velocity; // p(t + dt) = p(t) + dt * v(t + dt)
        p.force = Vec2f(0.f, 0.f);
    }
};

#endif


