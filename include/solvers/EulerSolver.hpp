#ifndef _EULERSOLVER_HPP_
#define _EULERSOLVER_HPP_

#include "api/Solver.hpp"

class EulerSolver: public Solver {
public:
    virtual ~Solver() {
    }

    //! Solve numerically the differential equation dp.velocity / dt = p.mass * p.force
    //! and update the position / velocity of the particle.
    virtual void solve(float dt, Particle& p) {
        p.position += dt * p.velocity; // p(t + dt) = p(t) + dt * v(t)
        p.velocity += dt * p.force / p.mass; // v(t + dt) = v(t) + dt * F / m
        p.force = Vec2f(0.f, 0.f);
    }
};

#endif

