cmake_minimum_required(VERSION 2.8)

project(MassSpringParticleSystem)

set(CMAKE_CXX_FLAGS "-Wall -ffast-math -std=c++0x")

find_package(OpenGL REQUIRED)
set(CMAKE_MODULE_PATH "/usr/local/share/SFML/cmake/Modules/" ${CMAKE_MODULE_PATH})
find_package(SFML 2.0 REQUIRED system window)

include_directories(${OPENGL_INCLUDE_DIRS} ${SFML_INCLUDE_DIRS} ${GLEW_INCLUDE_DIRS} include externals/include)
file(GLOB_RECURSE src_files src/*)

add_executable(MassSpringSystem ${src_files})

target_link_libraries(MassSpringSystem ${OPENGL_LIBRARY} ${SFML_LIBRARIES} GLEW)
