#include <iostream>
#include <GL/glew.h>
#include <SFML/Window.hpp>

#include "api/ParticleSystem.hpp"
#include "springs/NullSpring.hpp"

#include "renderer/GLRenderer.hpp"

using namespace glm;

int main() {
    sf::Window window(sf::VideoMode(512, 512), "Mass Spring Particle System");

    ParticleSystem system;

    NullSpring nullSpring;
    system.addSpring(&nullSpring);

    GLRenderer renderer;

    Particle p(Vec2f(0.f, 0.f), Vec2f(0.f, 0.f), Col3f(1.f ,1.f, 0.f), 1.f);

    while(window.isOpen()) {
        renderer.render(&p, 1);

        window.display();
        sf::Event e;
        while(window.pollEvent(e)) {
            if(e.type == sf::Event::Closed) {
                window.close();
                break;
            }
        }
    }

    return 0;
}
